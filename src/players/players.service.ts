import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { Player } from './player.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { SportDataGateway } from './sportdata.gateway';

@Injectable()
export class PlayersService {
  constructor(
    private readonly sportDataGateway: SportDataGateway,
    @InjectRepository(Player)
    private playersRepository: Repository<Player>,
  ) {}
  async create(data): Promise<any> {
    const player = this.playersRepository.create({
      player_id: data.PlayerID,
      status: data.Status,
      team: data.Team,
      jersey: data.Jersey,
      position: data.Position,
      first_name: data.FirstName,
      last_name: data.LastName,
      birth_date: data.BirthDate,
      birth_city: data.BirthCity,
      birth_state: data.BirthState,
      birth_country: data.BirthCountry,
      height: data.Height,
      weight: data.Weight,
      photo_url: data.PhotoUrl,
    });
    const playerSaved = await this.playersRepository.save(player);

    if (!playerSaved) {
      throw new InternalServerErrorException(
        'Problema ao criar player. Tente Novamente',
      );
    }

    return playerSaved;
  }

  async seed(): Promise<void> {
    const pĺayers = await this.find();
    console.log('Seeding is running');
    if (pĺayers.length === 0) {
      const list = await this.sportDataGateway.listPlayers();
      list.forEach(async (player) => {
        const playerById = await this.sportDataGateway.findByPlayerId(
          player.PlayerID,
        );
        await this.create({ ...player, PhotoUrl: playerById.PhotoUrl });
      });
    }
    console.log('Seeding running');
  }

  async find(): Promise<any> {
    const players = await this.playersRepository.find();
    return players;
  }
}
