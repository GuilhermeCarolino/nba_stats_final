import { Controller, Get } from '@nestjs/common';
import { PlayersService } from './players.service';

@Controller('players')
export class PlayersController {
  constructor(private readonly playersService: PlayersService) {}
  @Get()
  async list(): Promise<any> {
    const players = await this.playersService.find();
    return players;
  }
}
