import { Module } from '@nestjs/common';
import { PlayersService } from './players.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Player } from './player.entity';
import { SportDataGateway } from './sportdata.gateway';
import { HttpModule } from '@nestjs/axios';
import { PlayersController } from './players.controller';

@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([Player])],
  providers: [PlayersService, SportDataGateway],
  exports: [PlayersService],
  controllers: [PlayersController],
})
export class PlayersModule {}
