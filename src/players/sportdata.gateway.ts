/* eslint-disable prettier/prettier */
import { BadGatewayException, Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

@Injectable()
export class SportDataGateway {
  private readonly sportDataUrl: string;
  constructor(private readonly httpService: HttpService) {
    this.sportDataUrl = 'https://api.sportsdata.io/v3/nba';
  }

  static setHeader(): object {
    return {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    };
  }

  async listPlayers(): Promise<any> {
    try {
      const response = await this.httpService
        .get(
          `${this.sportDataUrl}/scores/json/Players?key=8ef5d26186ab4531bb1acc44bc3b74d3`,
          {
            headers: SportDataGateway.setHeader(),
          },
        )
        .toPromise();
      return response.data ? response.data : [];
    } catch (error) {
      throw new BadGatewayException('Problem with the bhut API');
    }
  }

  async findByPlayerId(id): Promise<any> {
    try {
      const response = await this.httpService
        .get(
          `${this.sportDataUrl}/scores/json/Player/${id}?key=8ef5d26186ab4531bb1acc44bc3b74d3`,
          {
            headers: SportDataGateway.setHeader(),
          },
        )
        .toPromise();
      return response.data ? response.data : [];
    } catch (error) {
      throw new BadGatewayException('Problem with the bhut API');
    }
  }
}
