/* eslint-disable prettier/prettier */
import { Expose } from 'class-transformer';
import { IsNotEmpty, IsString, IsNumber } from 'class-validator';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'players', orderBy: { id: 'ASC' } })
export class Player {
  @PrimaryGeneratedColumn()
  @Expose()
  id: string;

  @Column({ type: 'float', nullable: false, unique: true })
  @IsNotEmpty()
  @IsNumber()
  @Expose()
  player_id: number;

  @Column({ type: 'varchar', nullable: true })
  @IsString()
  @Expose()
  status?: string;

  @Column({ type: 'varchar', nullable: true })
  @IsString()
  @Expose()
  team?: string;

  @Column({ type: 'float', nullable: true })
  @IsNumber()
  @Expose()
  jersey?: number;

  @Column({ type: 'varchar', nullable: true })
  @IsString()
  @Expose()
  position?: string;

  @Column({ type: 'varchar', nullable: true })
  @IsString()
  @Expose()
  first_name?: string;

  @Column({ type: 'varchar', nullable: true })
  @IsString()
  @Expose()
  last_name?: string;

  @Column({ type: 'varchar', nullable: true })
  @IsString()
  @Expose()
  birth_date?: string;

  @Column({ type: 'varchar', nullable: true })
  @IsString()
  @Expose()
  birth_city?: string;

  @Column({ type: 'varchar', nullable: true })
  @IsString()
  @Expose()
  birth_state?: string;

  @Column({ type: 'varchar', nullable: true })
  @IsString()
  @Expose()
  birth_country?: string;

  @Column({ type: 'float', nullable: true })
  @IsNumber()
  @Expose()
  height?: number;

  @Column({ type: 'float', nullable: true })
  @IsNumber()
  @Expose()
  weight?: number;

  @Column({ type: 'varchar', nullable: true })
  @IsString()
  @Expose()
  photo_url: string;
}
