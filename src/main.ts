import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { PlayersService } from './players/players.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const playersService = app.get(PlayersService);
  playersService.seed();
  await app.listen(3000);
}
bootstrap();
