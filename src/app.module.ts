import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlayersModule } from './players/players.module';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '54.205.213.51',
      port: 5432,
      username: 'docker',
      password: 'ignite',
      database: 'rentx',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    PlayersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}


